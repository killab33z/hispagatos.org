+++
description = "OpenStreetMap Surveillance Traduccion."
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "OpenStreetMap Surveillance Traduccion"
date= 2019-03-07T12:10:12-03:00
images = [
  "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Surveillance_quevaal.jpg/200px-Surveillance_quevaal.jpg"
] # overrides the site-wide open graph image
+++

# Agregando camaras de vigilancia a Open Street Map- tutorial #

<center>![Camaras](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Surveillance_quevaal.jpg/200px-Surveillance_quevaal.jpg)</center>


## ¿Qué es Open Street Map? ##

Open Street Map es un mapa del mundo entero, puede ser editado por cualquiera y puede registrar un amplio conjunto de características. 
Es básicamente Wikipedia pero como un mapa, una de las cosas que puede registrar es la ubicación de la infraestructura de vigilancia, como las cámaras.


## Comencemos ##

Esta sección del tutorial asume que no tienes una cuenta de Open Street Map.

1. Dirigete a  (https://www.openstreetmap.org)
2. Haz click en el boton de registro "Sign Up"
3. Ingresa los detalles necesarios para crear tu cuenta


## Navengando ##

Esta sección del tutorial muestra cómo llegar a la parte del mundo donde se han encontrado cámaras de vigilancia.

1. Haga clic en el botón "Editar" en la parte superior izquierda de la pantalla del sitio web.
2. puede hacer clic en el walkthrough para obtener ayuda
3. La casilla "características de búsqueda" se encuentra en la parte izquierda del sitio web y es útil para navegar. 
4. Si tiene coordenadas geográficas, puede introducirlas en el campo "search features" box in the format _latitude,longitude_ como [decimal degrees](https://en.wikipedia.org/wiki/Decimal_degrees)



#### Alternativamente ####

1. Busca el lugar en el que se encuentra la cámara en la casilla "Funciones de búsqueda

## Editando ##

Esta sección muestra cómo editar el mapa y añadir cámaras de vigilancia que conozcas.

1. Haga clic en la opción "point" en la esquina superior izquierda del mapa
2. Haga clic en la ubicación en la que se encuentra la cámara
3. puede hacer clic y arrastrar el punto según sea necesario


## Agregar detalles ##

Esto muestra cómo decirle a la gente que se trata de una cámara de vigilancia y proporcionar información adicional que podría ser útil.

1. Una vez que añada el punto, verá un montón de diferentes tipos de características, así como un cuadro de búsqueda debajo del encabezado "seleccionar tipo de característica".
2. Haga clic en el cuadro de búsqueda y escriba "vigilancia".
3. Haga clic en la opción "cámara de vigilancia" que debe aparecer.
4. Hay un montón de cajas para rellenar con las diferentes características de la cámara, rellenar los detalles de acuerdo a su conocimiento

## Desglose de los distintos campos ##

Esta sección es para repasar las diferentes opciones de información a llenar y lo que significan.

1. Tipo de vigilancia: ¿Qué tipo de cámara de vigilancia es? Por ejemplo: ¿Es una cámara exterior? ¿Es en interiores? ¿Es de propiedad privada? Etc.
2. Tipo de vigilancia: ¿Qué es mirar? Básicamente, ¿es una cámara? ¿Es un guardia? ¿Es un lector de matrículas?
3. Tipo de cámara: ¿Qué tipo de cámara es? ¿Se queda en una posición? ¿Se mueve de un lado a otro? ¿Es una de esas cámaras domo en las que es difícil ver?
4. Montaje de la cámara: ¿A qué está conectada la cámara?
5. Dirección: ¿En qué dirección mira la cámara? Se mide en grados donde 0 es el norte.
6. Zona de Vigilancia: ¿Qué tipo de área está vigilando?
7. URL de la cámara web: Si es una webcam, ¿cuál es la URL a la que apunta la webcam?


## ¿Tiene información que no esté cubierta en estas opciones? ##

Podrías terminar teniendo información adicional sobre la cámara que sería útil conocer, pero que no aparece como un campo. Algunos campos adicionales están ocultos.

1. En la parte inferior de la pantalla, justo encima de la opción "All tags", hay un menú desplegable para "Add field". 
2. Al hacer clic en él, hay otras opciones útiles

## Algunas posibilidades de campos adicionales ##

Estos campos pueden ser útiles para agregar.

1. Fuentes: posiblemente para citas
2. Nivel: ¿En qué piso se encuentra la cámara? 0 es el nivel del suelo, los números negativos van más profundo en los pisos del sótano, los números positivos están por encima de la planta baja.
3. Notas: cualquier información adicional que considere relevante


#### Algunas ideas para anotar ###

1. ¿Qué marca y modelo es la cámara?
2. (Si es móvil) ¿Cuál es el rango de movimiento de la cámara?
3. ¿Cuál es el campo de visión de la cámara?


4. ¿Sabes de algún punto ciego?
5. ¿Sólo registra la luz visible o es una cámara térmica?
6. ¿Se procesa usando reconocimiento facial o AI?
7. ¿También graba audio?
8. ¿Hay más de una entrada del campo "tipo de vigilancia" que se ajuste a la descripción? Ponga los restantes aquí.

## Editar una segunda vez ##

Esto puede ser útil si, por ejemplo, encuentra nueva información sobre la cámara, o si la cámara ha sido sustituida por otra diferente.

1. Haga doble clic en el punto que ha creado
2. Cuando lo haga, deberá aparecer un banner que diga "editar característica" junto con los detalles que introdujo
3. Haga clic en la casilla situada justo debajo de la cabecera "editar característica

## Luego de terminar de editar las camaras ##

1. Haga clic en el botón "Guardar" en la esquina superior derecha del mapa
2. Continúe añadiendo o editando otras cámaras de las que tenga conocimiento


### Para mas información

- https://wiki.openstreetmap.org/wiki/Key:surveillance
- https://video.hispagatos.org
- https://www.openstreetmap.org

