---
title: "Jeremy_Hammond"
date: 2020-12-09T11:11:14+01:00
draft: false
author: "ReK2"
tags: ["hacking", "hackers", "cyberpunk", "Hispagatos", "Hacktivism"]
images: ["/images/jeremmy_hammond_big.jpg"]
---

![Jeremy Hammond](/images/jeremmy_hammond_big.jpg)

#### POR FIN LIBERADO!!!

Mas vale tarde que nunca, pero nuestro compa Jeremmy fue por fin liberado hace unas semanas.

La liberación del hacktivista anarquista se retrasó más de un año después de haber sido transferido arbitrariamente por un  programa de liberación anticipada a Alexandria, Virginia, para testificar ante un gran jurado. Cuando se negó a cooperar con la investigación de WikiLeaks.

Después de ocho años en una prisión federal, el compa Hacker Anarquista Jeremy Hammond por fin ha sido liberado. El grupo de defensa del Comité de Apoyo Jeremy Hammond tuiteó hace unas semanas  que había sido liberado de la Institución Correccional Federal de Tennessee, Memphis, a un centro de rehabilitación en Chicago, Illinois.

En solidaridad, desde Hispagatos, hicimos una donación junto a otr@s compañeros anarquistas.
En breve haremos un [Hackerñol](https://odysee.com/@Hackernol:7?r=2oKfdwxGsVSBPmRnPiWBAafmyjXPrpf4) con más detalles

### Contexto

En 2011, Hammond y un grupo de Hackers anarquistas del grupo LulzSec afiliado a Anonymous se abrieron camino en los únicos servidores de la empresa de contratación de inteligencia con sede en Texas Strategic Forecasting Inc, más conocida como Stratfor, llevandose más de 200 gigabytes de archivos, incluidos correos electrónicos, antes de destruir lo que quedaba de la base de datos de Stratfor.

La compañía proporcionó servicios de inteligencia privados para los autoritarios de la CIA y otras agencias de inteligencia de EE.UU. y la gran cantidad de documentos que LulzSec entregó a WikiLeaks reveló hasta qué punto la comunidad de inteligencia de EE.UU. se había entrelazado con las grandes empresas capitalistas y tecnologia de Sillicon Valley, para sortear las leyes de vigilancia nacionales.

Los archivos revelaron que Stratfor había sido contratado para espiar a los activistas del movimiento Occupy, en pleno  apogeo por todas las ciudades de los Estados Unidos, así como a grupos activistas como "People for the Ethical Treatment of Animals" (PETA), Anonymous y por supuesto, WikiLeaks.


- En la Lucha
- ReK2 - mastodon: @rek2@hispagatos.space
- 83l15 - mastodon: @isaveliskat@hispagatos.space
- https://hispagatos.space/@rek2
- https://hispagatos.space/@isaveliskat
- 
