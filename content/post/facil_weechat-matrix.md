+++
description = "Ya no hay escusas para no usar Weechat-matrix ;)"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "Weechat para Matrix con e2ee EDICIÓN FACIL"
date= 2019-11-24T03:30:21+01:00
images = [
  "https://source.unsplash.com/category/technology/1600x900"
] # overrides the site-wide open graph image
+++

![weechat](/images/weechat_example.png)

## Requerimientos
+ git

+ sudo

+ [yay](https://github.com/Jguer/yay)
```
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
```

+ Libolm 3.1+
```
yay -S libolm
```

+ [Pip](https://pip.pypa.io/en/stable/installing/)
```
yay -S python-pip

```

+ Weechat
```
yay -S weechat
```

+ Tener una cuenta en matrix.hispagatos.org. Si no la tienes, sigue [este tutorial](https://hispagatos.org/post/howto_matrix-riot/)

---
## Instalar el plugin de matrix para weechat
Se descarga el repositorio con
```
git clone https://github.com/poljar/weechat-matrix.git
```
y una vez dentro, hacemos
```
sudo pip install -r requirements.txt
```
para cubrir las dependencias de weechat-matrix.

Para instalar el paquete, se ejecuta con el usuario regular
```
make install
```
que instalará el fichero __main.py__ renombrado como __matrix.py__ en
__~/.weechat/python__ junto con los otros ficheros de python.

Si queremos que el plugin sea cargado al iniciar Weechat, habrá que hacer
```
mkdir ~/.weechat/python/autoload
ln -sf ~/.weechat/python/matrix.py ~/.weechat/python/autoload/matrix.py
```

Probablemente Weechat no cargue Python al iniciar Weechat. Eso es porque buscará
__libpython3.8.so.1.0__ o la versión que sea en __/usr/lib/__. Se soluciona haciendo
un link al __libpython3.so__ ya instalado en ese mismo directorio con
```
sudo ln -sv /usr/lib/libpython3.so /usr/lib/libpython3.8.so.1.0
```

---
## Establecer contraseñas
Una vez dentro de Weechat, empezaremos a configurarlo haciendo que se requiera una
contraseña para entrar a Weechat con
```
/secure passphrase <passwd>
```

y otra para entrar al server de Hispagatos (debe ser la contraseña de la cuenta de
matrix.hispagatos.org)
```
/secure set hispagatos_passwd <passwd>
```

Guardar los cambios
```
/save
```

---
## Connectar a matrix.hispagatos.org
Con la contraseña de Hispagatos ya establecida, podremos configurar el acceso a Hispagatos de forma
segura. 
Añadimos el servidor
```
/matrix server add hispagatos_matrix matrix.hispagatos.org
```
y fijamos nuestro nombre de usuario
```
/set matrix.server.hispagatos_matrix.username <username>
```
y la contraseña
```
/set matrix.server.hispagatos_matrix.password "${sec.data.hispagatos_passwd}"
```
Guardamos los cambios
```
/save
```

__"${sec.data.hispagatos_passwd}"__ contiene la contraseña creada en la sección de arriba.

Nos conectamos al servidor con
```
/matrix connect hispagatos_matrix
```

---
## Llaves
En el canal de Hispagatos usa llaves para encriptar los mensajes así que para poder hablar habrá
que verificar las llaves de los usuarios que ya se encuentran en la sala. La verificación gracias
a __libolm3__ es muy sencilla y podrás verificar las llaves de las siguientes formas:

- El servidor de hispagatos.org
```
/olm verify :hispagatos.org
```
- Un usuario en concreto del servidor hispagatos.org
```
/olm verify @nick:hispagatos.org
```
- El servidor de matrix.org
```
/olm verify :matrix.org
```
- Un usuario en concreto del servidor matrix.org
```
/olm verify @nick:matrix.org
```

Si ya tienes las llaves previamente exportadas en un fichero, puedes importarlas así:
```
/olm import /home/user/directorio/llaves.txt <contraseña del backup>
```

También puedes exportar las llaves de la siguiente forma:
```
/olm export /home/user/directorio/llaves.txt <contraseña del backup>
```

---
## Auto conectarse
Si queremos que al entrar a Weechat automaticamente nos conecte a Hispagatos, tendremos que
cambiar el valor de un una opción. Si en weechat haces
```
/fset hispagatos_matrix
```
se mostrarán todas las opciones referentes al servidor de Hispagatos. 
Una de esas es ```*.autoconnect``` y se activa con
```
/set matrix.server.hispagatos_matrix.autoconnect on
```
y se guardan los cambios
```
/save
```
Una vez hecho esto, cada vez que entres a Weechat, se autoconectará a Hispagatos.

<style>
    img {
        max-width: 100%;
    }
</style>
