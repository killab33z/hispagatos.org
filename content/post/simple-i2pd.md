+++
description = "Hispagatos To Public I2p IRC"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers", "i2p"]
title= "Simple I2pd"
date= 2018-03-20T22:09:15-07:00
images = [
  "https://i.ytimg.com/vi/XpFnmRa5B9c/maxresdefault.jpg"
] # overrides the site-wide open graph image
+++

##### HISPAGATOS TO I2P Migration - back to the roots

Now a days in just minutes, you will  be using i2p is very easy, better routers for example [i2pd](https://github.com/PurpleI2P/i2pd) instead of the bloated, old and annoying java router.

I am just going to enumerate the steps to get you into the **Public** **IRC** server, not our private IRC server nor our private .i2p sites.
If you are a good hacker you will not get enought of this steps, and actually read the original documentation and learn how to do everything, understand everything and then play with everything... (A)

* [Documentation](https://i2pd.readthedocs.io/en/latest/#invisible-internet-protocol-daemon)

1. Depending on your GNU/Linux distribution you can [find instructions](https://i2pd.readthedocs.io/en/latest/user-guide/install/) for PPA's and Debian repos.. 
  - Ubuntu: 
    
    ```
      sudo add-apt-repository ppa:purplei2p/i2pd
      sudo apt-get update
      sudo apt-get install i2pd
    ```
  - Debian:
    
    Add reporitory to /etc/apt/sources.list
      ```
        # Debian 7
        deb http://repo.lngserv.ru/debian wheezy main
        deb-src http://repo.lngserv.ru/debian wheezy main
        # Debian 8
        deb http://repo.lngserv.ru/debian jessie main
        deb-src http://repo.lngserv.ru/debian jessie main
      ```
    Import key that is used to sign the release:
      ```
        gpg --keyserver keys.gnupg.net --recv-keys 98EBCFE2
        gpg -a --export 98EBCFE2 | sudo apt-key add -
      ```
    After that you can install i2pd as any other software package:
      ```
        apt-get update
        apt-get install i2pd
      ```
  - Arch/BlackArch GNU/Linux:
    you need a package installed like trizen or pacaur that can pull packages [from the AUR](https://aur.archlinux.org/packages/i2pd/)
    here just two examples:
    ```
      trizen -S i2pd
      yaourt -S i2pd
    ```

  -  [NOTE] For CentoOS/Fedora/FreeBSD/Gentoo/Docker/Android etc please follow [the link](https://i2pd.readthedocs.io/en/latest/user-guide/install/) for the main documentation 


  - Now you can start the service, on systemd is:

    ```
      sudo systemctl start i2pd.service
      sudo systemctl enable i2pd.service // This is so starts when your server/computer starts (preferible) 

    ```
  - Install a IRC client, I recomend WeeChat
    - For Arch [Weechat Documentation](https://wiki.archlinux.org/index.php/WeeChat)
    - For Debian GNU/Linux [Weechat Packages](https://weechat.org/download/debian/)
    - For WeeChat [main documentation]https://weechat.org/doc/()



#### Connect to the Public IRC

 - Settings to connect to IRC for **weechat** or for another IRC client
    - Just like in TOR i2p uses **localhost/127.0.0.1** by default, most i2p users change this to point to servers connected by VPN and other means of ofuscation, RTFM for that.
    - So point your IRC to  **127.0.0.1**  port **6668**
    - Most IRC Clients you have to create a new network, for example i2p and then inside create the server to connect to 127.0.0.1
    - Here you can add settings/plugins etc.
    - Depending if you are planning in joining with your real persona or a fake/invented one make sure you hide anything that can give you up, read your IRC client manual and the i2pd documentation for details. if you just plan to come in as your RL persona then I guess it does not matter.


- ok now when you connect to the IRC, join this rooms:

  - #hispagatos
  - #Anarchism
  - #antifa
  - #leftsec
  - #latino-i2p
  - #SaltR
  - #Salt 

for more channels learn the IRC protocol and learn to run /list for a complete list of channels


##### Last note

*  Pay attention to this:
  - For **Android** I had to create some of the config files and upload to my phone so I could connect to IRC, there is a weechat app for
Android.
  - This is using default settings, the configs are usually under /etc/i2pd/ 
  - Learn about **i2p** **tunnels** if you want to connect to private servers/IrC later down the road.
  - I am just getting you into the **public IRC**, this is just touching the surface, i2p is a huge world with i2p sites, more IRC servers, Forums, email, torrents, etc etc RTFM 
  - To browse .i2p sites point a browser like **W3m** [CLI browsers](https://www.linuxhelp.com/how-to-install-command-line-browser-in-linux/)
  - If you still want to use your current GUI browser a tip is to use it from a script with a diff profile --profile etc something like:

    ```
      chromium --proxy-server="http://10.8.0.1:4444"  --incognito  --user-data-dir=~/path/to/my/hidden/persona/dir  my-site.i2p 
    ```

Hack The System a(A)a
ReK2 Hispagatos
