+++
description = "35c3-Chaos-Communication-Congress"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "35c5_Chaos_Communication_Congress"
date= 2019-02-04T15:44:35-08:00
images = [
  "https://fotos.hispagatos.org/upload/2019/02/04/20190204185306-380c65da.jpg"
] # overrides the site-wide open graph image
+++
-----
<center>![35c3 logo](https://fotos.hispagatos.org/upload/2019/02/04/20190204185306-380c65da.jpg "35c3 Logo")</center>

-----
35c3 Hispagatos Report back!
===========================

At hispagatos we were planning to attend one more year for what us is considered the best and still the truest 
hacker conference in the planet of that size, and years running. With only [HOPE](https://hope.net/) in NY behind it.

We organized with many other affinity groups under a common name called [1KOMONA](https://en.wikipedia.org/wiki/Kommune_1) in honor of left wing news paper building that the [CCC Chaos Computer Club](https://www.ccc.de/en/) was formed under.

```
The CCC was founded in Berlin on 12 September 1981 at a table which had previously belonged to the Kommune 1 in the rooms of the newspaper Die Tageszeitung by Wau Holland and others in anticipation of the prominent role that information technology would play in the way people live and communicatei
```

[Komona](https://www.komona.org/) is becoming an international collective of affinity collectives involved with the core ideas of 
changing the world with technology, hacking, social justice, music, dancing  and art. 
You can visit our [main site](https://www.komona.org/)

![Komona Picture](https://www.komona.org/AA_sticker_episode1_ohnetypo_downscale_h1200.png)

This are some of the collectives that were involved.
```
!decentral
F.U.C.K.
Freimeuter
Hispagatos - International Anarchist Hackers
Infoladen
Minibar
NoLog.cz IT kolektiv
Organ tempel
RCC Office
Reclaim Club Culture
Spieleberatung
```


The [Assembly of Komona at the CCC events site](https://events.ccc.de/congress/2018/wiki/index.php/Assembly:Komona) where you can see our collectives of activists, freedom fighters, 
anarchists and anarchist hackers, that had collaborative, informational and hands on events, with 
daily assembly's called: PL(A)NUM:

![Planum](https://fotos.hispagatos.org/_data/i/upload/2019/02/01/20190201023645-a13544c0-me.jpg)

![Anarchist hacker graffity](https://fotos.hispagatos.org/_data/i/upload/2019/02/01/20190201024122-fd30d31f-me.jpg)

![Komona Banner](https://fotos.hispagatos.org/_data/i/upload/2019/02/01/20190201023414-b8ab5ffb-me.jpg)



Our Assembly official post:

https://events.ccc.de/congress/2018/wiki/index.php/Assembly:Komona


-----
Our photo album site
====================
You can find more pictures at [our public Photo site](https://fotos.hispagatos.org/index.php?/category/25), note that most pics are done very late, before and after the event
to respect privacy of our comrades, but reading the banners and graffiti you can get an idea.

You will be able to find pictures of other events we participated, and random parties and hacker get together.




-----
Our Hackerñol recap video in Spanish!
==============================
Here is a 30m video of our recap in Spanish in our underground anarchist hacker video show
on our [peertube node](https://video.hispagatos.org/video-channels/c87b5aed-96db-4df5-8643-46507f0f41d4/videos)

<center>
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://video.hispagatos.org/videos/embed/08b7d267-dfd6-456d-b422-ffc7b7ad9e29" frameborder="0" allowfullscreen></iframe>
</center>

- The black bloc area

![Anarchist Hackers at 35c3](https://fotos.hispagatos.org/_data/i/upload/2019/02/01/20190201023342-698b01d6-me.jpg)


Happy Hacking, Hack the System and keep on dreaming! a(A)a <3
-------------------------------------------------------------

[ReK2](https://keybase.io/rek2)
[VideoShow](https://video.hispagatos.org/videos/local)
[Pictures](https://fotos.hispagatos.org)

---
