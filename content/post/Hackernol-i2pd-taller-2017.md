+++
date = "2017-05-26T14:10:27-07:00"
draft = false
title = "Hackernol i2pd taller 2017"

+++

**Hackerñol** - _servidor para la i2p Darknet esta vez con el_ 
_demonio/servicio nuevo de i2p llamado i2pd que esta escrito en C++_
_y no en JAVA por lo cual es mas rapido y mas actualizado._

_Donde bajar los ficheros que correr mientras se ve el video:_
[https://github.com/ChrisFernandez/tutorial-darknet](https://github.com/ChrisFernandez/tutorial-darknet)

**Diferencia entre el Cliente C++ al original de JAVA.**

Mientras Java I2P es un cliente original para la red I2P, 
i2pd tiene ciertas ventajas y grandes diferencias:
- Java I2P tiene aplicaciones integradas para torrents, correo electrónico y así sucesivamente. 
- I2pd es sólo un enrutador que puede utilizar con otro software a través de la interfaz I2CP. 
- I2pd no requiere Java. Está escrito en C ++. 
- I2pd consume menos memoria y CPU. 
- I2pd se puede compilar en todas partes con gcc incluyendo Rasperry PI y routers. 
- I2pd tiene algunas optimizaciones importantes que permite una criptografía más rápida 
que a lo cual resulta en un consumo menor de tiempo y energía del procesador

[seguirnos en twitter](https://twitter.com/anarcohacker)
[nuestro servidor mattermost](https://chat.binaryfreedom.info)
[en i2p IRC en el canal #hispagatos #leftsec #latino-i2p #anarchism](https://github.com/ChrisFernandez/tutorial-darknet)
**dentro de poco servidor "mastodon"**

_Credito de 10$ para Digital Ocean_
_Easily deploy an SSD cloud server on @DigitalOcean in 55 seconds_ 
_Sign up using my link and receive $10 in credit:_ [https://m.do.co/t/96e575af1f05](https://m.do.co/t/96e575af1f05)

<iframe width="560" height="315" src="https://www.youtube.com/embed/FaHGLRGqdcw" frameborder="0" allowfullscreen></iframe>
