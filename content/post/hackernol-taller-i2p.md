+++
date = "2016-09-24T16:21:57-07:00"
draft = false
title = "hackernol taller i2p"

+++

##### Conectar a la darknet I2P usando un servidor remoto, openvpn y weechat para IRC

**Estamos contentos de anunciar nuestro [primer taller](https://www.youtube.com/watch?v=JU5tGslFgpI) y recordaros sobre la  [website](https://xn--hackerol-i3a.net/) are live and kicking!**

*[I2P](https://geti2p.net/es/)
*[Ficheros en Github](https://github.com/ChrisFernandez/tutorial-darknet)


[![https://img.youtube.com/vi/zGnAWfCUJjU/0.jpg](https://img.youtube.com/vi/zGnAWfCUJjU/0.jpg)](https://www.youtube.com/watch?v=JU5tGslFgpI)

[Nuestra pagina web ***HACKERñOL***](https://xn--hackerol-i3a.net/)
##### Nuestras comunidades desde donde sale este programa:
* https://binaryfreedom.info
* https://hispagatos.org
* https://noisebridge.net/wiki/DC415
