+++
description = "Anarchist Hackers on CCC 2017"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "34_Chaos_communication_congress"
date= 2017-12-18T21:26:38-08:00
images = [
  "https://btctheory.files.wordpress.com/2017/06/hacktivism-anarchism.jpg"
] # overrides the site-wide open graph image
+++

## 34 Chaos Communication Congress

Some members of the _hispagatos_ _collective_ will be this year talking to old compas, new friends and future collaboratos and organizing having fun with other **anarchist** **hackers** at the CCC, prob the last real long going hacker conference after HOPE in NY that keeps it real.

We like to invite all other hackers, activists  or anarchist-hackers or whatever you feel you like, to join us.

You can find us here:

* https://events.ccc.de/congress/2017/wiki/index.php/Cluster:1Komona
* https://events.ccc.de/congress/2017/wiki/index.php/Assembly:Anarchist
* https://events.ccc.de/congress/2017/wiki/index.php/Assembly:Anarchist_Black_Cross

Come to our Matrix channel #hispagatos on matrix or the Anarchist hacker channel for the event here:

* https://matrix.to/#/#hispagatos:matrix.hispagatos.org
* https://riot.im/app/#/room/#34c3:telekollektiv.org

Of course we are in i2p IRC Channels #anarchism #hispagatos #latino-i2p


For while on the conference days, prob the best way to contact us is with keybase chat
add this one:

* https://keybase.io/cfernandez


We will be doing interviews for our hacker show and planning actions with our compas.

for more info: come to this blog or visit our reddit forums

* https://www.reddit.com/r/anarcho_hackers/
* https://www.reddit.com/r/HackBloc/


Our Mastodon server:

* https://hispagatos.space

Our Matrix server:

* https://matrix.hispagatos.org

DECENTRALIZE!!!
 
Hack the system a(A)a

ReK2
