+++
description = "Hispagatos joins coallition to save the net neutrality July 12 2017"
title = "Hispagatos joins coallition to save the net neutrality July 12 2017"
draft = false
toc = false
date = "2017-06-22T14:10:27-07:00"
categories = ["technology"]
tags = ["internet", "activism"]
images = [
  "https://www.battleforthenet.com/images/july12/02.jpg"
] # overrides the site-wide open graph image
+++

-On *July 12, 2017*, websites, Internet users, and online communities will come together to sound the alarm about the FCC?s attack on net neutrality. Learn how you can join the protest and spread the word at https://www.battleforthenet.com/july12/.

-Right now, new FCC Chairman and former Verizon lawyer Ajit Pai has a plan to destroy net neutrality and give big cable companies immense control over what we see and do online. If they get their way, the FCC will give companies like Comcast, Verizon, and AT&T control over what we can see and do on the Internet, with the power to slow down or block websites and charge apps and sites extra fees to reach an audience.

-If we lose net neutrality, we could soon face an Internet where some of your favorite websites are forced into a slow lane online, while deep-pocketed companies who can afford expensive new ?prioritization? fees have special fast lane access to Internet users ? tilting the playing field in their favor.

-But on July 12th, the Internet will come together to stop them. Websites, Internet users, and online communities will stand tall, and sound the alarm about the FCC?s attack on net neutrality.

-The Battle for the Net campaign will provide tools for everyone to make it super easy for your friends, family, followers to take action. From the SOPA blackout to the Internet Slowdown, we've shown time and time again that when the Internet comes together, we can stop censorship and corruption. Now, we have to do it again!

<p>Learn more and join the action here: https://www.battleforthenet.com/july12<p>
