+++
description = "Matrix Hispagatos"
draft = false
toc = true
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "Hackernol Especial Matrix"
date= 2017-11-22T16:39:51-08:00
images = [
  "https://source.unsplash.com/category/technology/1600x900"
] # overrides the site-wide open graph image
+++

##### Servidor Matrix de Hispagatos - Una Guia simple. #####

Un tutorial cortito de como hacerte una cuenta en nuestro servidor de hispagatos del protocolo matrix de chat descentralizado y de software abierto. 

* [El blog original de como hacerte una cuenta](https://hispagatos.org/blog/2017-06-26-hispagatos-federated-matrix-chat-node-howto)
* [Nuestro servidor MAtrix](https://matrix.hispagatos.org/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/VAwbl2Dfqw8" frameborder="0" allowfullscreen></iframe>


* FREE JEREMY HAMMOND!!!! Libertad para el compañero Jeremy Hammond.
 * [Freejeremy.net](https://freejeremy.net/)
 * [@FreeJeremyNet](https://twitter.com/FreeJeremyNet)

Musica por: 
* http://mixcloud.com/skittishandbus
* DualCore http://dualcoremusic.com/nerdcore @dualcoremusic

##### Gracias a #hispagatos #DC801, HackBloc , #dc415 a la #EFF #FSF y otros anarquistas hackers y anti autoritarios por compartir momentos inolvidables (A) #####

* [Nuestra pagina web](http://xn--hackerol-i3a.net/) 

##### Nuestras comunidades desde donde sale este programa: #####

* http://binaryfreedom.info
* https://hispagatos.org 
* https://noisebridge.net/wiki/DC415
* https://hispagatos.space 

##### los productores: #####

* [rek2](https://keybase.io/cfernandez)
* [Thibaud](https://keybase.io/thibaud_lopez)
