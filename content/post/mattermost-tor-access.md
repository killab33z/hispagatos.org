+++
date = "2016-08-15T00:33:25-07:00"
draft = false
title = "Chat with TOR access"

+++

* [chat.binaryfreedom.info](https://chat.binaryfreedom.info) and [chat.hispagatos.org](https://chat.hispagatos.org)  for **clearnet** access
* [https://o5igyzxaour43mbo.onion](https://o5igyzxaour43mbo.onion)  for **TOR** access

---

After asking <i>the collective and our community</i> we had an agreement to open **TOR** access to our [hispagatos/binaryfreedom chat rooms](https://chat.binaryfreedom.info)">hispagatos/binaryfreedom [invite only].
[Mattermost](https://www.mattermost.org) is a [Free Software](https://www.fsf.org/about/what-is-free-software) clone of Slack
we do not want to use Slack because is closed source, instead we rather use [Libre Software](https://www.fsf.org/about/what-is-free-software)

---

+ https://chat.binaryfreedom.info and https://chat.hispagatos.org  for **clearnet** access
+ https://o5igyzxaour43mbo.onion  for **TOR** access

<i>Hope this is useful!</i> 
##### Also compañeros remember joining our chat is invite only!!!

**En la lucha**
<br>
**ReK2**
