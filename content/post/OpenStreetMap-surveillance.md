+++
description = "Adding surveillance cams to OpenStreetMaps"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "Adding Surveillance cams to OpenStreet Maps tutorial"
date= 2019-03-04T19:01:50-08:00
images = [
  "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Surveillance_quevaal.jpg/200px-Surveillance_quevaal.jpg"
] # overrides the site-wide open graph image
+++

# Open Street Map surveillance cam - tutorial #

This tutorial will explain how to add surveillance cameras to Open Street Map.

<center>![Camaras](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Surveillance_quevaal.jpg/200px-Surveillance_quevaal.jpg)</center>

## What is Open Street Map? ##

Open Street Map is a map of the entire world. It can be edited by anyone and can record an extensive set of features. It is basically Wikipedia but as a map. One of the things it can record is the location of surveillance infrastructure such as cameras.

## Getting Started ##

This section of the tutorial assumes you don't have an Open Street Map account.

1. Go to (https://www.openstreetmap.org)
2. Click on the "Sign Up" button
3. Enter the required details to set up an account

## Navigating ##

This section of the tutorial shows how to get to the part of the world where the surveillance camera is found.

1. Click on the "Edit" button in the upper left side of the website screen
2. You can click on the walkthrough for help
3. The "search features" box is in the left side of the website and useful for navigating
4. If you have geographical coordinates, you could enter them in the "search features" box in the format _latitude,longitude_ as [decimal degrees](https://en.wikipedia.org/wiki/Decimal_degrees)

### Alternatively ###

1. search for the place the camera is in under the "search features" box

## Editing ##

This section is shows how to edit the map and add surveillance cameras you know about.

1. Click on the "point" option in the upper left corner of the map
2. Click on the location the camera is in
3. you can click and drag the point as needed

## Add Details ##

This shows how to tell people that it is a surveillance camera and provide extra information that might be useful to people.

1. Once you add the point you will see a lot of different types of features as well as a search box below the "select feature type" header
2. Click on the search box and type "surveillance"
3. click on the "surveillance camera option that should appear
4. There a a lot of boxes to fill out with different features of the camera, fill out the details according to your knowledge

## Breakdown of Different Fields ##

This section is to go over the different options for information to fill out and what they mean.

1. Surveillance Kind: What kind of surveillance camera is it? For example: Is it an outside camera? Is it indoors? Is it privately owned? Etc.
2. Surveillance Type: What is watching? Basically is it a camera? Is it a guard? is it a license plate reader?
3. Camera Type: What type of camera is it? Does it stay on one position? Does it move around? Is it one of those dome cameras that are difficult to see into?
4. Camera Mount: What is the camera attached to?
5. Direction: Which direction does the camera face? It is measured in degrees where 0 is north
6. Surveillance Zone: What type of area is it watching?
7. Webcam URL: If it is a webcam, what is the URL the webcam is pointing to?

## Do You Have Information Not Covered in These Options? ##

You could end up having extra information about the camera that would be useful to know, but that does not appear as a field. Some extra fields are hidden.

1. At the bottom of the screen, just above the "All tags" option, there's a drop down menu for "Add field" 
2. When you click on it, there are other useful options

## Some Possibilities of Extra Fields ##

These fields might be useful to add.

1. Sources: possibly for citations
2. Level: What floor is the camera found on? 0 is ground level, negative numbers go deeper into the basement floors, positive numbers are above the ground floor.
3. Notes: any additional information you find relevant

### Some Ideas to Put as Notes ###

1. What brand and model is the camera?
2. (If mobile) What is the camera's range of motion?
3. What is the camera's field of view?
4. Do you know about any blind spots?
5. Does it only record visible light, or is it a thermal camera?
6. Is it processed using facial recognition or AI?
7. Does it also record audio?
8. Are there more than one entries from the "surveillance kind" field that fit the description? Put the remaining ones here

## Edit a Second Time ##

This might be useful if you, for example, find out new information about the camera, or if the camera has been replaced by a different one
1. Double click on the point you created
2. When you do, a banner saying "edit feature" should appear along with details you entered
3. Click on the box just below the "edit feature" header

## After You Finish Editing the Camera ##

1. Click on the "save" button in the upper right corner of the map
2. Continue adding or editing other cameras you found out about

### For more information
- https://wiki.openstreetmap.org/wiki/Key:surveillance
- https://video.hispagatos.org
- https://www.openstreetmap.org
