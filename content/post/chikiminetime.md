+++
description = "Help support Hispagatos by mining"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "cryptocurrency", "hispagatos"]
title= "Help support Hispagatos by mining"
date= 2020-03-11T18:05:07-05:00
+++

# HOW TO - MINE LIKE A GATO
Here is a quick tutorial on how to help support Hispagatos by running cryptocurrency mining software.

## Getting Started

Welcome to a step-by-step Monero (XMR) miner tutorial brought to you by your local gatos y gatas. This was tested with Arch Linux *meow* and updated last on March 11, 2020.

### Installing & Initial Setup

1) Install MoneroOcean's xmrig fork from AUR with yay.
```
	$ yay -S xmrig-mo
```

2) The mining information we have is:
```
	Mining pool:	gulf.moneroocean.stream
	Mining port:	10001
	Mining address: 4AYprKT26fnZYdY1JjKLMR2K7zmCQhe9i5bn5oMV4VtEYvjiFhhXLVq8iycHRLTr6cD3oVSq19Yrx4JfmuuaU453M8GzTKb
``` 

3) Now to configure xmrig.
```
	$ sudo cp /etc/xmrig-mo/xmrig-mo.conf.example /etc/xmrig-mo/xmrig-mo.conf
	$ sudo vim /etc/xmrig-mo/xmrig-mo.conf
```

4) Enter the info we have in the config file as follows:
- Change "url" to our Mining pool:Mining port.
- Change "user" to the Mining address.
- Change "pass" to the miner name you want.
- Change "coin" to null (no quotes).
- Change "algo" to null (no quotes).
- Keep the other values in "pools" section unchanged.

Here is a snippet example below with the above mentioned changes:
```
    "pools": [
        {
            "algo": null,
            "coin": null,
            "url": "gulf.moneroocean.stream:10001",
            "user": "4AYprKT26fnZYdY1JjKLMR2K7zmCQhe9i5bn5oMV4VtEYvjiFhhXLVq8iycHRLTr6cD3oVSq19Yrx4JfmuuaU453M8GzTKb",
            "pass": "killab33z",
            "rig-id": null,
            "nicehash": false,
            "keepalive": true,
            "enabled": true,
            "tls": false,
            "tls-fingerprint": null,
            "daemon": false,
            "self-select": null
        }
    ],

```

We set algo and coin both to null so, according to [MoneroOcean's FAQ] (https://moneroocean.stream/user/help/faq.html), the pool will auto switch to mine Monero, and other similar coins, and will still yield Monero in the end. RTFM their FAQ for more info.

Also, on [Reddit] (https://www.reddit.com/r/MoneroMining/comments/f9p3q3/moneroocean_pool_now_support_mining_to_onion/fitv02q/) a MoneroOcean admin states:
```
"Algo switching works by modifying miner so it will benchmark algos on your system and provide this algo perf data to the pool, so pool can always give your miner the best profit coin jobs. Pool uses exchange to auto trade all coins to monero to pay it to miners."
```

5) Set your desired log file location in the config (below with example):
```
	"log-file": "/tmp/xmrig-mo.log",
```

6) Now to setup your miner to use a CPU and/or a GPU to mine. *I have only tested xmrig CPU mining only, so RTFM.*

### Setup CPU Mining

For the CPU miner setup, make sure the following is contained in the config:
```
	"cpu": {
		"enabled": true,
		
	"opencl": {
		"enabled": false,
		
	"cuda": { 
		"enabled": false,	
```

### Setup GPU Mining

Since I do not have any GPUs to test now, I used the new [XMRig Configuration Wizard](https://xmrig.com/wizard)  to learn what needs to be changed. The following are my findings, but RTFM to be sure:

#### AMD GPU

For AMD-based GPU, make sure the following is contained in the config:

```
	"opencl": {
		"enabled": true,
		
	"cuda": { 
		"enabled": false,	
```

#### Nvidia GPU

For Nvidia-based GPU, make sure the following is contained in the config:

```
	"opencl": {
		"enabled": false,
		
	"cuda": { 
		"enabled": true,	
```

### Start the miner

First run with sudo so that the results of the automatic algorithm performance tests get written to the conf file:
```
	$ sudo xmrig-mo --config=/etc/xmrig-mo/xmrig-mo.conf
```

Afterwards, you can kill it and run it as user:
```
	$ xmrig-mo --config=/etc/xmrig-mo/xmrig-mo.conf
```

***
### My personal tweaks

* With the latest version of xmrig (5.9.x) a new algo named "astrobwt" was added. I have noticed that this algo performs terrible on my CPUs, so I change it from whatever number is in the "algo-perf" conf file section to 0.0.
```
	$ sudo vim /etc/xmrig-mo/xmrig-mo.conf
```
```
	"algo-perf": {
		"astrobwt": 0.0
	},
```

* I will add other tweaks here as I discover them ^_^

***
### And then...

- To see it in the mining pool, go to https://moneroocean.stream/#/dashboard and add the above Mining (XMR) address and you can see your miner and the stats, etc.

- Also, I believe you can mine using both the CPU and the GPU with xmrig at the same time, but need someone to confirm if this is or is not possible.

- If you need to re-perform xmrig's algorithm performance tester, then change "rebench-algo" to true in the xmrig config, restart xmrig, and then change the xmrig config back to false after.

- You can also adjust the port number used based upon your hardware via [MoneroOcean's port list](https://moneroocean.stream/#/ports).

***
### Anything else...

If there is anything else I missed then let me know. As always -> RTFM, hack the system & enjoy life! - Killab33z  
https://hispagatos.org

