+++
description = "Guerrilla Open Access Manifesto - Guerrilla del acceso libre a la informacion"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "Guerrilla del acceso libre a la información"
date= 2019-09-22T17:02:02+02:00
images = [
  "https://covers.openlibrary.org/w/id/7361987-L.jpg"
] # overrides the site-wide open graph image
+++

![aaron](/images/aaron_swartz.jpeg)

# Guerilla Open Access Manifesto - Guerrilla del acceso libre a la informacion

Información es poder. Pero como todo poder, hay quienes quieren conservarlo para sí mismos. 
Todo el patrimonio científico y cultural del mundo, publicado durante siglos en libros y revistas, 
esta siendo, cada vez más, digitalizado y privatizado por un puñado de corporaciones privadas.
¿Quieres leer los documentos con los resultados más famosos de la ciencias? 
Entonces tendrás que enviar enormes cantidades [dinero] a editoriales como Reed Elsevier.

Hay quienes luchan por cambiar esto. El Open Access Movement ha luchado valientemente para garantizar 
que los científicos no firmen con derechos de autor [sus trabajos], sino que se aseguren que su trabajo 
se publica en Internet, bajo términos que permitan que cualquiera pueda acceder a estos.
Pero en el mejor de los casos, esto solo se aplicará a los trabajos futuros.
Todo hasta ahora se ha perdido.

Ese es un precio demasiado alto para pagar. ¿Obligar a los académicos a pagar para leer el trabajo de 
sus colegas? ¿Escanear bibliotecas enteras pero solo permitiendo que la gente de Google las lea?
¿Proporcionar artículos científicos a aquellos en universidades de élite del Primer Mundo, 
pero no a niños en el sur del globo? Es indignante e inaceptable.

“Estoy de acuerdo”, dicen, “pero ¿qué podemos hacer? Las compañías poseen los derechos de autor, 
hacen enormes cantidades de dinero cobrando por el acceso [a los documentos], y es perfectamente legal -- 
no hay nada que podamos hacer para detenerlos". 
En realidad, hay algo que si podemos hacer, algo que ya se está haciendo: podemos contraatacar.

Aquellos con acceso a estos recursos -- estudiantes, bibliotecarios, científicos -- tienen un privilegio. 
Puedes alimentarte en este banquete de conocimiento mientras que el resto del mundo está vetado. 
Pero no necesitas -- de hecho, moralmente, no puedes -- mantener este privilegio para ti mismo.
Tienes el deber de compartir con el mundo. Tienes qué: intercambiar contraseñas comerciales con colegas, 
saciar solicitudes de descarga para amigos.

Mientras tanto, aquellos que han sido dejados fuera no están de brazos cruzados. 
Tú has estado colándote por agujeros y saltando vallas, liberando la información cerrada 
por los editores y compartiéndola con tus amigos.

Pero toda esta acción continúa en la oscuridad, oculta del público. 
Es llamado robo o piratería, como si compartir una gran cantidad de conocimiento fuera el equivalente 
moral de saquear un barco y asesinar a la tripulación. Compartir no es inmoral -- es un imperativo moral. 
Solamente aquellos cegados por la codicia se negarían a dejar que un amigo se hiciera una copia. 

Las grandes compañias, por supuesto, están cegadas por la codicia. Las leyes bajo las cuales operan 
lo requieren -- sus accionistas se rebelarían por menos. Y los políticos han sido comprados, aprobando 
leyes que les otorgan el exclusivo poder de decidir quien puede hacer copias. 

**No hay justicia en seguir leyes injustas**. Es hora de salir a la luz y, en el la gran tradición de 
la desobediencia civil, declarar nuestra oposición a esta privatización de la cultura pública.

**Necesitamos** conseguir información, donde sea que esté almacenada, hacer nuestras copias y compartirlas con el mundo.<br>
**Necesitamos** obtener materiales que no están protegidas por derechos de autor y agregarlas al [archivo](https://archive.org/).<br> 
**Necesitamos** comprar bases de datos secretas y ponerlas en la Web.<br> 
**Necesitamos** descargar diarios científicos y subirlos a redes de intercambio de archivos.<br>
**Necesitamos** luchar por el Guerilla Open Access.<br>

Con suficientes de nosotros, en todo el mundo, no solo enviaremos un fuerte mensaje en oposición a la 
privatización del conocimiento -- haremos que sea cosa del pasado. 
¿Te unirás?

Aaron Swartz
Julio de 2008, Eremo, Italia

Traducido por MegageM.

<style>
	img {
		max-width: 100%;
	}
</style>
