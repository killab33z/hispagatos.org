#example commands to create new posts test draft and build
#
##

#Pull submodules (the themes)
git submodule update --init --recursive
##
hugo new post/new-post-name.md
hugo server --buildDrafts -t terminal
hugo undraft content/post/ctf_linuxfestnorthwest.md

# CHECK if the .md file removed the DRAF=True to DRAF=False
# if not, then edit and remove it and git add
#hugo -t terminal #this is optinal now since gitlab will take care of this

git add content/post/name_blog_post.md
git commit "I added new blogpost about..."
git push

# keep submodules updated
git submodule update --recursive --remote
